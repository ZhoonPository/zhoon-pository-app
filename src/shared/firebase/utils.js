export const extractDataWithDocumentId = (document) => {
  const data = document.data();
  return {
    ...data,
    id: document.id,
  }
};
