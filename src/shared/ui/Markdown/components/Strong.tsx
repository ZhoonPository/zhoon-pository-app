import React from "react";

type Props = {
  value: string;
};
const Strong: React.FC<Props> = (props) => {
  return <strong className="blogStrong">{props.value}</strong>;
};

export default Strong;
