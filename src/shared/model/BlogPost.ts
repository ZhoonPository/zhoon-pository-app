import FirestoreDocument from "./FirestoreDocument";
import firebase from "firebase";
export interface BlogPost extends FirestoreDocument {
  contentJson: string;
  userId: string;
  name: string;
  deleted: boolean;
  blogFile: string;
  blogId: string;
  dateCreated: firebase.firestore.Timestamp;
  imageUrl: string;
}

export interface Blog {
  contentJson: string;
  userId: string;
  name: string;
  deleted: boolean;
  blogFile: string;
  blogId: string;
  imageUrl: string;
  dateCreated: firebase.firestore.Timestamp;
}
