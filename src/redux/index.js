import { combineReducers } from 'redux'
import blogs from '../modules/blogs/redux/reducer';
const rootReducer = combineReducers({
    blogs:blogs,
})


export default rootReducer;